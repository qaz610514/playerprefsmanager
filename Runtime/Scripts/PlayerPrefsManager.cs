﻿namespace com.FunJimChee.Jason.PlayerPrefsTools
{
    using UnityEngine;
    using System;
    using Newtonsoft.Json;

    public static class PlayerPrefsManager
    {
        public static (bool result, T data) Load<T>(string key) where T : class, new()
        {
            try
            {
                var json = PlayerPrefs.GetString(key);

                if (string.IsNullOrEmpty(json)) return (false, default);

                var data = JsonConvert.DeserializeObject<T>(json);

                return (true, data);
            }
            catch (Exception e)
            {
                Debug.LogWarning(e);
                return (false, default);
            }
        }

        public static void Save<T>(string key, T data) where T : class, new()
        {
            var json = JsonConvert.SerializeObject(data);

            if (string.IsNullOrEmpty(json)) return;

            PlayerPrefs.SetString(key, json);
        }

        public static void Delete(string key)
        {
            PlayerPrefs.DeleteKey(key);
        }
    }

}