﻿namespace com.FunJimChee.Jason.PlayerPrefsTools.Example
{
    using UnityEngine;

    public class Demo : MonoBehaviour
    {
        class DemoClass
        {
            public string Name;
            public string Message;
        }

        // Start is called before the first frame update
        void Start()
        {
            //Save Data
            var data = new DemoClass { Name = "yoyo", Message = "我好兇～" };
            PlayerPrefsManager.Save("Example", data);

            //Load Data
            var (result, getData) = PlayerPrefsManager.Load<DemoClass>("Example");
            Debug.Log(result ? $"{result} => {getData.Name} say : {getData.Message}" : $"Load fail!");

            //Delete Data
            PlayerPrefsManager.Delete("Example");
        }
    }

}